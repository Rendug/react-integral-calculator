import './App.css';
import AnswerItem from './AnswerItem.js';
import { Component } from 'react';
import jQuery from 'jquery';
import React from 'react';

class App extends Component{
  
  constructor (props){
    super(props);

    this.state={
      answerList:[],
      a:0, 
      b:100, 
      N:100,
      parameterList:[]
    }

    this.defaultInputColor = 'white';
    this.errorInputColor = 'red';

    this.refA = React.createRef();
    this.refB = React.createRef();
    this.refD = React.createRef();

    this.onAChange = this.onAChange.bind(this);
    this.onBChange = this.onBChange.bind(this);
    this.onNChange = this.onNChange.bind(this);
    this.deleteAllHandler=this.deleteAllHandler.bind(this);
    this.calculateHandler=this.calculateHandler.bind(this);
  }

  deleteHandler(index){
    let answerList = this.state.answerList;
    let parameterList = this.state.parameterList;
    answerList.splice(index,1);
    parameterList.splice(index, 1);
    this.setState({answerList:answerList, parameterList: parameterList});
  }

  deleteAllHandler()
  {
    const answerListNew=[];
    const parameterListNew=[];
    this.setState({answerList:answerListNew, parameterList:parameterListNew});
  }

  onAChange(e) {
    this.refA.current.style.background = this.defaultInputColor;
    let val = e.target.value;
    this.setState({a: val });
  }

  onBChange(e) {
    this.refB.current.style.background = this.defaultInputColor;
    let val = e.target.value;
    this.setState({b: val });
  }

  onNChange(e) {
    this.refD.current.style.background = this.defaultInputColor;
    let val = e.target.value;
    this.setState({N: val });
  }

  // calculateHandler=()=>{
  //   let currentList = this.state.answerList;
  //   let result = this.calculateIntregal();
  //   currentList.push({answer:result});
  //   this.setState({answerList:currentList})
  // }

  resetFormState()
  {
    this.refA.current.style.background = this.defaultInputColor;
    this.refB.current.style.background = this.defaultInputColor;
    this.refD.current.style.background = this.defaultInputColor;
  }

  validateForm()
  {
    var success = true;

    if(this.refA.current.value === "" || isNaN(+this.refA.current.value))
    {
      success = false;
      this.refA.current.style.background = this.errorInputColor;
    }

    if(this.refB.current.value === "" || isNaN(+this.refB.current.value))
    {
      success = false;
      this.refB.current.style.background = this.errorInputColor;
    }

    if(this.refD.current.value === "" || !(this.refD.current.value % 1 === 0) || (+this.refD.current.value) <= 0)
    {
      success = false;
      this.refD.current.style.background = this.errorInputColor;
    }

    if(this.refA.current.value * 1 > this.refB.current.value * 1)
    {
      success = false;
      this.refA.current.style.background = this.errorInputColor;
      this.refB.current.style.background = this.errorInputColor;
    }

    return success;
  }

  calculateHandler=()=>
  {
    this.resetFormState();

    var result = this.validateForm();

    if(!result)
    {
      return;
    }

    let currentList = this.state.answerList;

    let requestBody = 
    {
      function: "(ln(x) * x) ^ 2",
      integralBegin: this.state.a,
      integralEnd: this.state.b,
      numberOfPartisions: this.state.N
    }

    let app = this;

    jQuery.ajax({
      type: 'POST',
      url: 'http://localhost:5000/api/integral',
      beforeSend: function(request) {
        request.setRequestHeader("Content-Type", "application/json");
      },
      data: JSON.stringify(requestBody)
    }).done(function(data)
    {
      currentList.push({answer:data});
      app.state.parameterList.push("LB = " + requestBody.integralBegin + "; RB = " + requestBody.integralEnd + "; DC = " + requestBody.numberOfPartisions);
      app.setState({answerList:currentList});
    });
  }

  calculateIntregal()
  {
    let b = this.state.b;
    let a = this.state.a; 
    let n = this.state.N;
    let h = (b - a) / n;
    let sum = 0;

    this.state.parameterList.push("LB = " + a + "; RB = " + b + "; DC = " + n);

    for(let i = 1; i < n; i++)
    {
      let xiCurrent = a * 1 + h * i * 1;
      let xiLast = a * 1 + h * (i - 1) * 1;
      sum += this.func((xiLast + xiCurrent) / 2) * (xiCurrent - xiLast);
    }

    return sum;
  }

  func(x)
  {
    return Math.pow((Math.log(x) * x), 2);
  }

  render(){
    let test = this.state.answerList.map((ans,index)=>{
      if(index === this.state.answerList.length - 1)
      {
        return(<AnswerItem style={{backgroundColor: "#8eeef7"}} answer = {ans.answer} 
          parameters = {this.state.parameterList[index]}
          onDelete = {this.deleteHandler.bind(this, index)}/>);
      }
      else
      {
        return(<AnswerItem style={{backgroundColor: "#00A3B3"}} answer = {ans.answer} 
          parameters = {this.state.parameterList[index]}
          onDelete = {this.deleteHandler.bind(this, index)}/>);
      }
    }).reverse();
    
    return (
      <div className="settings">
        <h1 className="header">Integral Calculator</h1>
        <div className="inputField">
          <div>
            <p>Left Border</p><input ref={this.refA} type = "text" onChange={this.onAChange}></input>
          </div>
          <div>
            <p>Right Border</p><input ref={this.refB} type = "text" onChange={this.onBChange}></input>
          </div>
          <div>
            <p>Divisions Count</p><input ref={this.refD} type = "text" onChange={this.onNChange}></input>
          </div>
        </div>
        <br />
        <div style={{marginTop: "15px", marginBottom: "15px"}}>
          <button className="formButton formButtonHover" onClick = {this.calculateHandler}>Calculate</button>
          <button className="formButton formButtonHover" onClick ={this.deleteAllHandler}>Clear</button>
        </div>
        { test }   
      </div>
    );
  }
}

export default App;
