import { Component } from "react";
import chartImage from '../Resources/chart.png';

class Chart extends Component{
    
    render (){
        return( 
        <div>
            <h1>Chart</h1>
            <img src={chartImage} />
        </div>
        );
    }
}

export default Chart;