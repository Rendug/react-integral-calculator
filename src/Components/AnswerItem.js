import { Component } from "react";

class AnswerItem extends Component{
    
    render (){
        return( <div className="answerItem hoverShadow" style={this.props.style}>
            <button className="deleteButton" onClick={this.props.onDelete}>X</button>
            <p>Result: {this.props.answer}</p>
            <p>Parameters: {this.props.parameters}</p>
        </div>
        );
    }
}

export default AnswerItem;