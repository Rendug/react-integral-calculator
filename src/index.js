import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Pages from './Components/Pages.js';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";

ReactDOM.render((
  <BrowserRouter>
    <Pages />
  </BrowserRouter>
  ), document.getElementById('root'),
);

reportWebVitals();
